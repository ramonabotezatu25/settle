export interface IState {
    userContext: IUserContext
    restaurants: IRestaurant[],
}

export interface IUserContext {
    isUserLoggedIn: boolean,
    authRedirectPath: string,
    username: string,
    token: string,
    shouldRegister: boolean,
    errorMessage: string,
    startAuth: boolean,

}

export interface IRestaurant {
    address: string,
    description: string,
    hours: IOpeningHours,
    id: number,
    logo: string,
    name: string,
    phone_number: string,
    review: string,
    type: string,
    uid: string,
}

export interface IOpeningHours {
    [key: string]: IOpeningInfo
}

export interface IOpeningInfo {
    closes_at: string,
    is_closed: boolean,
    opens_at: string,
}