import React, {FunctionComponent} from 'react';
import {Box, IconButton, ImageListItem, Modal} from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import {IOpeningInfo, IRestaurant} from "../../utils/interfaces/IState";
import Typography from "@mui/material/Typography";
import CancelIcon from "@mui/icons-material/Cancel";

interface IProps {
    isOpen: boolean;
    closeModal: () => void;
    restaurant: IRestaurant;
    currentDay?: string;
    openingHours: IOpeningInfo;
}

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const ModalComponent: FunctionComponent<IProps> = ({
  isOpen = false,
  closeModal,
  restaurant,
  currentDay,
  openingHours,
}) => {

    return (
        <>
            <Modal
                open={isOpen}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Box sx={style}>
                    <IconButton
                        aria-label="delete"
                        size="small"
                        onClick={closeModal}
                    >
                        <CancelIcon
                            sx={{
                                position: 'absolute',
                                left: '500px',
                                top: '-25px'
                            }}
                        />
                    </IconButton>
                    <Typography
                        variant="h6"
                        component="div"
                        align={'center'}
                    >
                        {restaurant?.name}
                    </Typography>
                    <ImageListItem
                        key={restaurant.uid}
                        sx={{
                            width: 500,
                            height: 150}}
                    >
                        <img
                            style={{height: 300}}
                            src={restaurant.logo}
                            srcSet={restaurant.logo}
                            alt={restaurant.name}
                            loading="lazy"
                        />
                    </ImageListItem>
                    <Typography
                        sx={{textAlign: 'center'}}
                        gutterBottom
                        variant="body2"
                        component="div"
                        align={'center'}
                    >
                        <b>Review: </b>
                        {restaurant?.review}
                    </Typography>
                    <Typography
                        gutterBottom
                        variant="body2"
                        component="span"
                        align={'center'}
                    >
                        <b>Opening hours </b> {openingHours?.opens_at} - {openingHours?.closes_at}
                    </Typography>
                    <Typography
                        gutterBottom
                        variant="body2"
                        align={'left'}
                    >
                        <b>Open Today ({currentDay}): </b>
                        {openingHours?.is_closed ? 'CLOSED' : "OPEN"}
                    </Typography>
                </Box>
            </Modal>
        </>
    );
};

export default ModalComponent;