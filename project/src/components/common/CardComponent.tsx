import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {IOpeningInfo, IRestaurant} from "../../utils/interfaces/IState";
import {FunctionComponent} from "react";
import {getDayOfTheWeek} from "../../utils/functions/dateTime";
import {ButtonBase} from "@mui/material";
import ModalComponent from "./ModalComponent";


interface IProps {
    data: IRestaurant,
}

const CardComponent: FunctionComponent<IProps> = ({
 data,
}) => {

    const currentDay = getDayOfTheWeek();
    const openingHours: IOpeningInfo = data?.hours[currentDay?.toLowerCase()];
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <>
            <Card sx={{maxWidth: 500, maxHeight: 500, padding: '10px'}}>
                <ButtonBase sx={{display: 'block'}} onClick={handleOpen}>
                    <CardMedia
                        component="img"
                        height="140"
                        image={data?.logo}
                        alt="green iguana"/>

                        <CardContent>
                            <Typography
                                gutterBottom
                                variant="h6"
                                component="div"
                            >
                                {data?.name}
                            </Typography>
                            <Typography
                                gutterBottom
                                variant="body2"
                                component="div"
                                align={'center'}
                            >
                                {data?.type}
                            </Typography>
                            <Typography
                                sx={{
                                    display: '-webkit-box',
                                    overflow: 'hidden',
                                    WebkitBoxOrient: 'vertical',
                                    WebkitLineClamp: 5,
                                }}
                                gutterBottom={true}
                                variant="body2"
                                color="text.secondary"
                            >
                                {data?.description}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Typography
                                gutterBottom
                                variant="body2"
                                component="span"
                                align={'left'}
                            >
                                Open Today ({currentDay})
                                <br/>
                                {openingHours?.opens_at} - {openingHours?.closes_at}
                            </Typography>
                            <Typography
                                sx={{
                                    marginLeft: '80px',
                                }}
                                variant="body1"
                                align={'left'}
                                color={openingHours?.is_closed ? 'red' : 'green'}
                            >
                                {openingHours?.is_closed ? 'CLOSED' : "OPEN"}
                            </Typography>
                        </CardActions>
                    </ButtonBase>
                </Card>
                <ModalComponent
                    isOpen={open}
                    closeModal={handleClose}
                    restaurant={data}
                    currentDay={currentDay}
                    openingHours={openingHours}
                />
            </>
        );
    };

export default CardComponent;