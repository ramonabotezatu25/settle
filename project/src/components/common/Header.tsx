import React from 'react';
import './styles.css';
import {Button} from "@mui/material";
import {useDispatch} from "react-redux";
import UserActionCreator from "../../redux/userContext/actionCreator";


const Header =  () => {
    const dispatch = useDispatch();
    const logout = () => {
        dispatch(UserActionCreator.logout());
    };

    return (
         <div className="header">
             <Button
                 variant="contained"
                 size={'medium'}
                 onClick={logout}
             >
                 Logout
             </Button>
         </div>
    );
};

export default Header;