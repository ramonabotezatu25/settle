import React, {FunctionComponent, useEffect, useState} from 'react';
import './styles.css'
import CardComponent from "../common/CardComponent";
import { connect } from 'react-redux';
import {IRestaurant, IState} from "../../utils/interfaces/IState";
import {CircularProgress} from "@mui/material";
import Box from "@mui/material/Box";

interface IProps {
    restaurants: IRestaurant[],
}

const RestaurantsList: FunctionComponent<IProps> = ({
   restaurants,
}) => {
    const [loading, setLoading] = useState(false);
    const formattedRestaurants  = Object.values(restaurants);


    useEffect(() => {
        if (restaurants.length === 0) {
            setLoading(true)
        } else {
            setLoading(false);
        }
    }, [restaurants]);

    return (
        <div className="list-container">
            {loading ?
                <Box sx={{ display: 'block', position: 'relative', top: '30%'}}>
                    <CircularProgress />
                </Box>
                :
                <div className='lists'>
                    {formattedRestaurants?.map((restaurant, index) => {
                        return <CardComponent  key={`restaurant-${index}`}data={restaurant}/>
                    })}
                </div>
            }
        </div>
    );
};

function mapStateToProps(state: IState) {
    return {
        restaurants: state.restaurants,
    }
}
export default connect(mapStateToProps)(RestaurantsList);