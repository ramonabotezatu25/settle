import React, {Component} from 'react';
import {BrowserRouter} from "react-router-dom";
import { Routes ,Route } from 'react-router-dom';

import Restaurants from "../../pages/Restaurants";
import LoginPage from "../../pages/LoginPage";

class MainLayout extends Component {
    render() {

        return (
            <BrowserRouter>
                <Routes>
                    <Route
                        path={"/"}
                        element={<LoginPage/>}
                    />
                    <Route
                        path={"/login/"}
                        exact={true}
                        element={<LoginPage/>}
                    />
                    <Route
                        exact={true}
                        path={"/restaurants/"}
                        element={<Restaurants/>}
                    />
                    <Route element={<LoginPage/>}/>
                </Routes>
            </BrowserRouter>
        );
    }
}

export default MainLayout;
