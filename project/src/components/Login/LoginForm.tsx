import React, {FunctionComponent, useEffect, useState} from 'react';
import {Button, CircularProgress, TextField} from "@mui/material";
import './styles.css';
import {useDispatch} from "react-redux";
import UserActionCreator from "../../redux/userContext/actionCreator";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";


interface IProps {
    shouldRegister: boolean,
    errorMessage: string,
    startAuth: boolean,
}

const LoginForm: FunctionComponent<IProps> = ({
   shouldRegister = false,
   errorMessage,
   startAuth,
}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    const dispatch = useDispatch();

    const handleSubmit = (e: any) => {
        e.preventDefault();

        const payload = {
            email: email,
            password,
        };

        if (shouldRegister) {
            dispatch(UserActionCreator.register(payload));
        } else {
            dispatch(UserActionCreator.authenticate(payload));
        }
        dispatch(UserActionCreator.startAuthentication());
    };

    useEffect(() => {
            setLoading(startAuth)
        }, [startAuth])

    return (
        <form className='form' onSubmit={handleSubmit}>
            {errorMessage &&
            <Typography
                gutterBottom
                variant="body2"
                component="span"
                align={'center'}
                color={'yellow'}
            >
                {errorMessage}
            </Typography>
            }
            <TextField
                label="Email"
                variant="filled"
                type="email"
                required
                value={email}
                onChange={e => setEmail(e.target.value)}
            />
            <TextField
                label="Password"
                variant="filled"
                type="password"
                required
                value={password}
                onChange={(e: { target: { value: React.SetStateAction<string>; }; }) => setPassword(e.target.value)}
            />
            <div>
                <Button
                    type="submit"
                    variant="contained"
                    color={shouldRegister? 'secondary' : 'primary'}
                >
                    {shouldRegister ? "Register" : "Login"}
                </Button>
            </div>
            {loading &&
                <Box sx={{ display: 'block', position: 'relative', top: '30%'}}>
                    <CircularProgress />
                </Box>
            }
        </form>
    );
};


export default LoginForm;