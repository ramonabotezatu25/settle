import axios from 'axios';

const setupInterceptors = () => {
    axios.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        // Do something with response error
        if (error.response) {
            return Promise.reject(error.response);
        }
        return Promise.reject(error.message);
    });
};

const interceptors = {
    setupInterceptors,
};
export default interceptors;