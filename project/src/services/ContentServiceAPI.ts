import firebase from "firebase/compat";

const firebaseConfig = {
    apiKey: "AIzaSyDz1robnK54n54pfowZJXt3ri-m0xm1Xs8",
    authDomain: "settleproj.firebaseapp.com",
    projectId: "settleproj",
    storageBucket: "settleproj.appspot.com",
    messagingSenderId: "824317512256",
    appId: "1:824317512256:web:51bc6158054e9bf3712da5"
};

const app = firebase.initializeApp(firebaseConfig);
const auth = app.auth();
const db = app.firestore();


export const signInWithEmailAndPassword = async (email = " ", password = " ") => {
    try {
        const res = await auth.signInWithEmailAndPassword(email, password);
        const user = res.user;
        await db.collection("users").add({
            uid: user?.uid,
            authProvider: "local",
            email,
        });
        return res;
    } catch (err) {
        console.error("login Error", err);
    }
};

export const registerWithEmailAndPassword = async (email = " ", password = " ") => {
    try {
        const res = await auth.createUserWithEmailAndPassword(email, password);
        const user = res.user;
        await db.collection("users").add({
            uid: user?.uid,
            authProvider: "local",
            email,
        });
        return res;
    } catch (err) {
        console.error("register Error", err);
    }

};

export const logout = () => {
    return auth.signOut();
};

