import axios from "axios";

const getRestaurantsData = (token: string) => {
    if (!token) {
        return new Error("Token is null or invalid");
    }

    return axios.get("https://random-data-api.com/api/restaurant/random_restaurant?size=100", {
        headers: {
            'Content-Type': 'application/json',
        }
    });
};


const DataServiceAPI = {
    getRestaurantsData,
};
export default DataServiceAPI;