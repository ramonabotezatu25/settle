import React, {Fragment} from 'react';
import PropTypes from "prop-types";
import {Switch, Route} from "react-router-dom";

import getRoutes from "./routes";
import LoginPage from "../pages/LoginPage";


class AppRouter extends React.Component {

    render() {
        const routes = getRoutes();
        return (
            <Fragment>
                <Switch>
                    {routes.map((route, index) => {
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                exact={false}
                                component={route.component}
                            />
                        )
                    })}
                    {
                        <Route component={LoginPage}/>
                    }
                </Switch>
            </Fragment>
        );
    }
}

AppRouter.propTypes = {
    component: PropTypes.func,
    componentKey: PropTypes.string,
    defaultComponent: PropTypes.func

};
export default AppRouter;
