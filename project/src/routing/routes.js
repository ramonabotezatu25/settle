import LoginPage from "../pages/LoginPage";
import Restaurants from "../pages/Restaurants";
import React from "react";

export const defaultRoute = {
    path: '',
    main: LoginPage
};

export default function getRoutes() {
    return [
        {
            path: '/restaurants',
            element: <Restaurants/>
        },
        {
            path: '/login',
            element: <LoginPage/>,
        },
        defaultRoute,

    ]
}
