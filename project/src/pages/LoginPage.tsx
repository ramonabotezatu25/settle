import React, {FunctionComponent} from 'react';
import LoginForm from "../components/Login/LoginForm";
import '../components/Login/styles.css'
import {connect} from "react-redux";
import {IState} from "../utils/interfaces/IState";
import {Navigate} from "react-router";

interface IProps {
    shouldRegister: boolean,
    isUserLoggedIn: boolean,
    authRedirectPath: string,
    errorMessage: string,
    startAuth: boolean,
}


const LoginPage: FunctionComponent<IProps> = ({
 shouldRegister,
 isUserLoggedIn,
 authRedirectPath,
 errorMessage,
 startAuth
}) => {

    let shouldRedirect = null;
    if (authRedirectPath !== '/' && isUserLoggedIn) {
        shouldRedirect = <Navigate to={authRedirectPath}/>
    }

    return (
        <div className='loginPage'>
            {shouldRedirect}
            <LoginForm startAuth={startAuth} shouldRegister={shouldRegister} errorMessage={errorMessage}/>
        </div>
    );
};

function mapStateToProps({userContext}: IState) {
    return {
        shouldRegister: userContext.shouldRegister,
        isUserLoggedIn: userContext.isUserLoggedIn,
        authRedirectPath: userContext.authRedirectPath,
        errorMessage: userContext.errorMessage,
        startAuth: userContext.startAuth,
    }
}
export default connect(mapStateToProps)(LoginPage)
