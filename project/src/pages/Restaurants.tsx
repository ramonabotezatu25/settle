import React, {FunctionComponent} from 'react';
import {connect, useDispatch} from "react-redux";
import RestaurantActionCreator from "../redux/restaurants/actionCreator";
import Header from "../components/common/Header";
import RestaurantsList from "../components/restaurants/RestaurantsList";
import {Navigate} from "react-router";
import {IState} from "../utils/interfaces/IState";


interface IProps {
    isUserLoggedIn: boolean,
}

const Restaurants:FunctionComponent<IProps> = ({
 isUserLoggedIn,
}) => {
    const dispatch = useDispatch();
    let shouldRedirect = null;
    if (!isUserLoggedIn) {
        shouldRedirect = <Navigate to={'/login'}/>
    }
    dispatch(RestaurantActionCreator.loadRestaurants());

    return (
        <div>
            {shouldRedirect}
            <Header/>
            <RestaurantsList/>
        </div>
    );
};
function mapStateToProps({userContext}: IState) {
    return {
        isUserLoggedIn: userContext.isUserLoggedIn,
    }
}
export default connect(mapStateToProps)(Restaurants);