import './App.css';
import React from "react";
import MainLayout from "./components/MainContent/MainContent";
import * as actionTypes from "./redux/actionName";
import {useDispatch} from "react-redux";

const App = () => {
    const dispatch = useDispatch();
    dispatch({type: actionTypes.USER_AUTH_CHECK_STATE});

    let route = '/login/';
    let path = document.location.href.split("/");
    if (path.length && path[3]) {
        const resultingRoute = path.slice(3);
        route = "/" + resultingRoute.join("/");
    }

    return (
        <div className="App">
            <MainLayout route={route}/>
        </div>
    );
};


export default App;
