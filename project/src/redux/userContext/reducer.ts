import * as actionTypes from '../actionName'
import {initialState} from "../initialstate";
import {IUserContext} from "../../utils/interfaces/IState";

const authenticate = (state: IUserContext, payload: any) => {
        return {
            ...state,
            ...payload,
            shouldRegister: false,
            isUserLoggedIn: true,
            authRedirectPath: "/restaurants/",
            startAuth: false,
        }
};

const authenticationFail = (state: IUserContext, payload: any) => {
    return {
        ...state,
        ...payload,
        isUserLoggedIn: false,
        authRedirectPath: "/login/",
        shouldRegister: true,
        errorMessage: payload?.message,
        startAuth: false,
    }
};

const startAuthentication = (state: IUserContext, payload: any ) => {
    return {
        ...state,
        ...payload,
        startAuth: true,
    }
};

const logoutSucces = () => {
    return initialState.userContext;
};

export default function reducer (state = initialState.userContext, action: any) {
    const {type, payload} = action;
    switch(type) {
        case actionTypes.USER_AUTHENTICATION_SUCCESS: return authenticate(state, payload);
        case actionTypes.USER_AUTHENTICATION_FAIL: return authenticationFail(state, payload);
        case actionTypes.USER_START_AUTHENTICATION: return startAuthentication(state, payload);
        case actionTypes.USER_LOGOUT_SUCCESS: return logoutSucces();
        default: return state;
    }
}
