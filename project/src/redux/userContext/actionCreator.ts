import * as actionTypes from "../actionName";


const authenticate = (payload: { email: string, password: string }) => {
    return {
        type: actionTypes.USER_AUTHENTICATION,
        payload: payload
    }
};

const authSuccess = (payload: any) => {
    return {
        type: actionTypes.USER_AUTHENTICATION_SUCCESS,
        payload: payload,
    }
};

const authFail = (payload: any, message: string) => {
    return {
        type: actionTypes.USER_AUTHENTICATION_FAIL,
        payload: {
            payload,
            message,
        },
    }
};

const register = (payload: { email: string, password: string }) => {
    return {
        type: actionTypes.USER_REGISTER,
        payload: payload,
    }
};

const logout = () => {
    return {
        type: actionTypes.USER_LOGOUT,
    }
};

const startAuthentication = () => {
    return {
        type: actionTypes.USER_START_AUTHENTICATION,
    }
};

const UserActionCreator = {
    authenticate,
    authSuccess,
    authFail,
    register,
    logout,
    startAuthentication,
};

export default UserActionCreator;