import {put} from "redux-saga/dist/redux-saga-effects-npm-proxy.esm";
import RestaurantsFacade from "../../facade/restaurantsFacade";
import RestaurantActionCreator from "../restaurants/actionCreator";

export function* getRestaurantsList() {
    try {
        const token =  yield localStorage.getItem("token");
        const response = yield RestaurantsFacade.getRestaurantData(token);
        yield put(RestaurantActionCreator.loadRestaurantsSuccess(response));
    } catch (err) {
        console.error(err);
    }
}
