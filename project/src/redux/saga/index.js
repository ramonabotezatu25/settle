import {all, takeLatest, takeEvery} from "redux-saga/dist/redux-saga-effects-npm-proxy.esm";
import * as actionTypes from "../actionName";
import {getRestaurantsList} from "./restaurant-saga";
import {authCheckStateSaga, authUserSaga, checkAuthTimeoutSaga, logoutUserSaga, registerUserSaga} from "./userSaga";

export function* watchStudentActions() {
    yield all([
        takeLatest(actionTypes.LOAD_RESTAURANTS_LIST, getRestaurantsList),
    ]);
}


export function* watchUserActions() {
    yield all([
        takeLatest(actionTypes.USER_AUTHENTICATION, authUserSaga),
        takeLatest(actionTypes.USER_REGISTER, registerUserSaga),
        takeEvery(actionTypes.USER_AUTH_CHECK_STATE, authCheckStateSaga),
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
        takeEvery(actionTypes.USER_LOGOUT, logoutUserSaga),
    ]);

}
