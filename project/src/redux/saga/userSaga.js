import { put, delay } from "redux-saga/dist/redux-saga-effects-npm-proxy.esm";
import UserActionCreator from "../userContext/actionCreator";
import UserFacade from "../../facade/userFacade";

    function* startAuthenticationProcess(token, email) {
        try {
            const userContext = {
                token: token,
                email: email,
            };

            yield localStorage.setItem("token", userContext.token);
            yield localStorage.setItem("username", userContext.email);
            yield localStorage.setItem("expirationTime", new Date().getTime().toString());


            yield put(UserActionCreator.authSuccess(userContext));

        } catch(err) {
            console.error("LOGIN", err);
        }

    }

    export function* authUserSaga(action) {
        const {email = ' ', password} = action.payload;
        try {
            const result = yield UserFacade.login(email, password);
            if(result?.token) {
                yield startAuthenticationProcess(result?.token, email);
            } else {
                yield put(UserActionCreator.authFail(-1, result?.err));
            }
        }catch(err) {
            yield put(UserActionCreator.authFail(-1, err?.toString()));
        }
    }

    export function* registerUserSaga(action) {
        const {email = ' ', password} = action.payload;
        try {
            const result = yield UserFacade.register(email, password);
            if  (result?.token) {
                yield startAuthenticationProcess(result?.token, email);
            } else {
                yield put(UserActionCreator.authFail(-1, result?.err));
            }
        }catch(err) {
            yield put(UserActionCreator.authFail(-1, err?.toString()));
        }
    }

    export function* authCheckStateSaga() {
        const token = yield localStorage.getItem("token");
        const expirationTime = yield localStorage.getItem("expirationTime");
        if (!token) {
            yield put(UserActionCreator.logout());
            yield logoutUserSaga();
            yield localStorage.clear();
        } else {
            const currentTime = yield new Date().getTime();
            if (parseInt(currentTime) - expirationTime >= 10000 ) {
                // 900000
                yield put(UserActionCreator.logout());
                yield logoutUserSaga();
                yield localStorage.clear();
            } else {
                yield startAuthenticationProcess(token);
            }
        }
}


    export function* logoutUserSaga() {
        yield localStorage.clear();
        yield UserFacade.logout();
    }

    export function* checkAuthTimeoutSaga(action) {
        yield delay(action.expirationTime * 10000);
        yield put(UserActionCreator.logout());

    }
