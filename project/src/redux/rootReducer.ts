import {combineReducers} from 'redux';
import restaurants from './restaurants/reducer'
import userContext from './userContext/reducer'

const rootReducer = combineReducers({
    restaurants,
    userContext,

});

export default rootReducer;
