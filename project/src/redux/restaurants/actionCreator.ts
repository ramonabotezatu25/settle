import * as actionTypes from "../actionName";

export default {
    loadRestaurants () {
        return {
            type: actionTypes.LOAD_RESTAURANTS_LIST,
        }
    },

    loadRestaurantsSuccess(payload: any) {
        return {
            type: actionTypes.LOAD_RESTAURANTS_LIST_SUCCESS,
            payload: payload,
        }

    }
}