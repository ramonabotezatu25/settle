import * as actionTypes from '../actionName'
import {initialState} from "../initialstate";
import {IRestaurant} from "../../utils/interfaces/IState";

const saveRestaurants = (state: IRestaurant[], payload: any) => {
        return {
            ...state,
            ...payload,
        }
};

export default function reducer (state = initialState.restaurants, action: any) {
    const {type, payload} = action;
    switch(type) {
        case actionTypes.LOAD_RESTAURANTS_LIST_SUCCESS: return saveRestaurants(state, payload)
        default: return state;
    }
}
