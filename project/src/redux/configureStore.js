import {createStore, applyMiddleware, compose} from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./rootReducer";
import {
    watchStudentActions,
    watchUserActions,
} from "./saga";

export default function configureStore(preloadedState) {

    const sagaMiddleware = createSagaMiddleware();
    let composeEnhancers;

    if (process.env.NODE_ENV === "development") {
        composeEnhancers =
            (window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ?
                window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({trace: true, traceLimit: 25}) :
                compose ? compose: null
    } else {
        composeEnhancers = compose ? compose : null
    }

    const middleware = [sagaMiddleware];

    const store = createStore(
        rootReducer,
        preloadedState,
        composeEnhancers(applyMiddleware(...middleware))
    );

    sagaMiddleware.run(watchStudentActions);
    sagaMiddleware.run(watchUserActions);

    if (process.env.NODE_ENV !== 'production') {
        if (module.hot) {
            module.hot.accept('./rootReducer', () => {
                store.replaceReducer(rootReducer);
            });
        }
    }

    return store;

}
