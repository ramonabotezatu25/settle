import {IState} from "../utils/interfaces/IState";

export const initialState: IState = {
    userContext: {
        isUserLoggedIn: false,
        authRedirectPath: "/login",
        username: '',
        token: '',
        shouldRegister: false,
        errorMessage: '',
        startAuth: false,
    },
    restaurants: [],
};
