import DataServiceAPI from "../services/DataServiceAPI";

class RestaurantsFacade {
    static getRestaurantData(token) {
        return DataServiceAPI.getRestaurantsData(token)
            .then(data => data.data);
    }
}

export default RestaurantsFacade;

