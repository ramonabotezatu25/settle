import {signInWithEmailAndPassword, registerWithEmailAndPassword, logout} from "../services/ContentServiceAPI";

class RestaurantsFacade {
    static login(email, password) {
        return signInWithEmailAndPassword(email, password)
            .then((resp) => {
                if (resp) {
                    return resp?.user?.getIdToken().then((token) => {
                        return {
                            token: token,
                            err: null,
                        }
                    });
                } else {
                    return {
                        token: null,
                        err: "Something went wrong! Try again",
                    }
                }
            }).catch((err) => {
                return {
                    token: null,
                    err: err?.toString(),
                }
            });
    }

    static register(email, password) {
        return registerWithEmailAndPassword(email, password)
            .then((resp) => {
                if (resp) {
                    return resp?.user?.getIdToken().then((token) => {
                        return {
                            token: token,
                            err: null,
                        }
                    });
                } else return {
                    token: null,
                    err: "Something went wrong! Try again",
                }

            }).catch((err) => {
                return {
                    token: null,
                    err: err?.toString(),
                }
            })
    }

    static logout() {
        return logout()
            .then((resp) => {
                return resp;
            })
    }
}

export default RestaurantsFacade;

